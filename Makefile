CFLAGS=-Wall

all: clean
	$(CC) $(CFLAGS) uninfinity.c -o uninfinity

clean:
	rm -f uninfinity

mem: all
	valgrind --leak-check=full --show-leak-kinds=all -s uninfinity

win:
	rm -f uninfinity.exe
	x86_64-w64-mingw32-gcc uninfinity.c -o uninfinity.exe

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

bool isANumber(char number[])
{
	
	unsigned i = 1;
	
	if( number[0] == '-' )
		i = 2;

	for(; i <= strlen(number); ++i)
	{
		if(!(isdigit(number[i-1]) || number[i-1] == '.'))
				return false;
	}

	return true;
}

short subtract_one(char number, char* result)
{
	switch(number)
	{
		case '1':
			*result = '0';
			break;
		case '2':
			*result = '1';
			break;
		case '3':
			*result = '2';
			break;
		case '4':
			*result = '3';
			break;
		case '5':
			*result = '4';
			break;
		case '6':
			*result = '5';
			break;
		case '7':
			*result = '6';
			break;
		case '8':
			*result = '7';
			break;
		case '9':
			*result = '8';
			break;
		case '0':
			*result = '9';
			return 1;
			break;
		case '.':
			break;
		default:
			printf("error: subtract_one: number argument is not a digit or a point char!\n");
			return -1;
	}

	return 0;
}

void invert_number(char* number, char* output)
{
	unsigned length = strlen(number);
	for(int i = length-1; i >= 0; --i)
	{
		output[length-1-i] = number[i];
	}
	output[length] = '\0';
}

void uninfinity(char* input, char* output)
{
	unsigned length = strlen(input);
	char output_tmp[length];
	unsigned short carry[length];

	for(int i = length-1; i >= 0; --i)
	{
		if(i == length-1)
		{
		  	output_tmp[i] = input[i]; // units remain untouched
			continue;
		}

		carry[i] += subtract_one(input[i], &output_tmp[i]);
		
		while(!(carry[i+1] == 0))
		{
			carry[i] += subtract_one(output_tmp[i], &output_tmp[i]);
			--carry[i+1];
		}

	}
	output_tmp[length] = '\0';

	if(carry[0] == 1)
		strcpy(output, &output_tmp[1]);
	else if(carry[0] == 0)
		strcpy(output, output_tmp);
	else
	{
		printf("This should not have happened. There is a big error in the code!\n");
		exit(-99);
	}

}

int main(int argc, char* argv[])
{

	char *number_arg = argv[1];

	/* argument number check */

	if(!(argc == 2))
	{
		printf("error: I only receive one number to uninfinity.\n");
		return -1;
	}

	/* check if argument is a number */
	
	if(!(isANumber(number_arg)))
	{
		printf("error: I only accept a number to uninfinity.\n");
		return -2;
	}

	/* check if argument is a number */
	
	if( !(strlen(number_arg) == 1) && number_arg[0] == '0' )
	{
		printf("error: I only accept a decimal number (no zeros to the left).\n");
		return -3;
	}

	/* do the uninfinitation */

	/* separate floating point part of the number */
	bool floating;
	char *point_char = strchr(number_arg, '.');
	char *floating_number = NULL;
	if(!(point_char == NULL))
	{
		floating_number = ++point_char;
		floating = true;

		--point_char;
		*point_char = '\0';
	}

	/* separate signal and integer number */

	bool negative;
	char *natural_number;
	if (number_arg[0] == '-')
	{
		negative = true;
		natural_number = &number_arg[1];
	}
	else 
	{
		negative = false;
		natural_number = number_arg;
	}


	/* begin uninfinitation processing */

	unsigned length = strlen(natural_number);
	char result_natural_number[length];
	uninfinity(natural_number, result_natural_number);
	

	/* join signal and number */
	
	char signal;
	if(strcmp(result_natural_number, "0") == 0)
	{
		printf("0\n");
		return 0;
	}
	
	signal = (negative) ? '-' : '+';
	if(floating)
		printf("%c%s.%s\n", signal, result_natural_number, floating_number);
	else
		printf("%c%s\n", signal, result_natural_number);

	return 0;
}

# Uninfinity

 - Have you ever wonder how the decimal numerical system looks like if the zero was not an infinite number?
 - Do you dislike the infinite?
 - Do you think zeros to the left should count?
 - Have you notice that a infinite zero causes an abnormal number sequence in the decimal numerical system?

Well, then this program is made just for you. Read below ;)

## Build

This program is written in C. You need a C compiler (gcc?) and the program `make`. Now run:
```
make
```

## Arguments

This program is `uninfinity` (it should be on this project's directory).

This program receives only one argument, and the argument has to be a positive or negative natural number of the decimal numerical system.

As such:
```
uninfinity <number-from-the-decimal-system>
```

## What does it do?

The output of the program is the decimal number, if the decimal numerical system would not have an infinite zero.

So the result is the number, if 00 is diferent than 0 and if 000 is diferent than 0 and if 0000 is diferent than 0, etc.. In programming terms: 00 != 0 and 000 != 0 and 0000 != 0, etc..

For a better understanding, try to run `uninfinity` with the following arguments, one at a time: `9` , `10` , `11` .

## Last message

Enjoy numbers and consider looking at my other repositories that target numerical systems.

Also, consider buying or building an abacus. It is an inspiration for all of my numbers work.

## LICENSE

See LICENSE file.
